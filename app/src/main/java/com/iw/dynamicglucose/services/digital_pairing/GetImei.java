
package com.iw.dynamicglucose.services.digital_pairing;


public class GetImei {

    private String imei;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

}
