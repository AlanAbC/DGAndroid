package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.iw.dynamicglucose.services.user_profile.Measure;

import java.util.List;

/**
 * Created by IW on 15/02/2018.
 */

public class AdapterEditProfile extends BaseAdapter {
    private LayoutInflater inflator;
    private List<Measure> measures;

    AdapterEditProfile(Context context, List<Measure> measures){
        inflator = LayoutInflater.from(context);
        this.measures = measures;
    }

    @Override
    public int getCount() {
        return measures.size();
    }

    @Override
    public Object getItem(int i) {
        return measures.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final Measure measure = measures.get(i);
        View edit_view = inflator.inflate(R.layout.cell_edit_profile, null);
        TextView title = edit_view.findViewById(R.id.txt_cell_edit_profile);
        title.setText(measure.getName());
        final EditText input = edit_view.findViewById(R.id.input_cell_edit_profile);
        input.setHint(measure.getValue());
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                measure.setValue(input.getText().toString());
            }
        });
        return edit_view;
    }
}
