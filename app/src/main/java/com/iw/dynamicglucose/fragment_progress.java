package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.do_daily_check.DoDailyCheck;
import com.iw.dynamicglucose.services.get_weight.GetWeight;
import com.iw.dynamicglucose.services.view_progress_program.ViewProgramProgress;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class fragment_progress extends Fragment {
    private LineChart lineChart;
    private Button setWeightBtn;
    private TextView lblProgress;
    private MaterialDialog dialog;
    private BottomNavigationView bottomNavigationView;

    //loaders
    private MaterialDialog dialogDays;
    private MaterialDialog dialogWeight;
    private MaterialDialog dialogPermission;

    private Services services;
    private UserSession user;
    private Session session;

    //vars weight
    private String weightValue;
    private String valueInteger;
    private String valueDecimal;
    private ImageView back_button_meals;

    public fragment_progress() {
        // Required empty public constructor
    }

    public static fragment_progress newInstance() {
        return new fragment_progress();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_progress, container, false);
    }
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);

        //init config vars
        session = new Session(getView().getContext());
        if(session.getCodeNotif() != 0){
            session.setCodeNotif(0);
        }
        user = session.getSession();
        Service service = new Service();
        services = service.getService();

        //init layout vars
        lineChart = getView().findViewById(R.id.lineChart);
        CircleImageView imageView = getView().findViewById(R.id.imageusr_progrs);
        ImageButton imageButton = getView().findViewById(R.id.refresh);
        setWeightBtn = getView().findViewById(R.id.set_weight);
        lblProgress = getView().findViewById(R.id.lbldays);
        back_button_meals = getView().findViewById(R.id.back_button_meals);
        bottomNavigationView = ((Home)getActivity()).findViewById(R.id.navigation);

        back_button_meals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuItem item = bottomNavigationView.getMenu().getItem(0);
                item.setChecked(true);
                fragment_home fragment = fragment_home.newInstance();
                FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment);
                ft.commit();
            }
        });

        //init var weight value
        weightValue = "";
        valueInteger = "";
        valueDecimal = "";

        if(!user.getAvatar().equals("")){
            Picasso.with(getView().getContext()).load(user.getAvatar()).into(imageView);
        }


        loadWeight();
        loadProgramProgress();
        getPermissions();

        //listener of refresh
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadWeight();
            }
        });
        setWeightBtn.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {

                dialog = new MaterialDialog.Builder( getContext())
                        .customView(R.layout.weigh_picker, false)
                        .build();
                dialog.show();
                View viewDialog = dialog.getCustomView();
                assert viewDialog != null;
                NumberPicker intWeight = viewDialog.findViewById(R.id.spinner_weight_int);
                intWeight.setMinValue(100);
                intWeight.setMaxValue(400);
                intWeight.setWrapSelectorWheel(true);
                intWeight.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                        valueInteger = String.valueOf(i1);
                    }
                });

                NumberPicker decimalWeight = viewDialog.findViewById(R.id.spinner_weight_decimals);
                decimalWeight.setMinValue(0);
                decimalWeight.setMaxValue(9);
                decimalWeight.setWrapSelectorWheel(true);
                decimalWeight.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                        valueDecimal = String.valueOf(i1);

                    }
                });

                NumberPicker libs = viewDialog.findViewById(R.id.spinner_weight_libs);
                libs.setMinValue(0);
                libs.setMaxValue(0);
                libs.setDisplayedValues(new String[] {"libs."});

                Button registerWeight = viewDialog.findViewById(R.id.register_new_weight_button);
                registerWeight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (valueInteger.equals("")) {
                            valueInteger = "132";
                        }

                        if(valueDecimal.equals("")) {
                            valueDecimal = "0";
                        }

                        weightValue = valueInteger + "." + valueDecimal;

                        setWeight();
                    }
                });
            }
        });
    }
    private void loadWeight() {

        dialogWeight = loader();
        dialogWeight.show();
        // Create the call of the service time to start
        Call<GetWeight> call = services.get_weight(user.getId());

        // Executing call of the service time to start
        call.enqueue(new Callback<GetWeight>() {
            @Override
            public void onResponse(Call<GetWeight> call, Response<GetWeight> response) {
                switch (response.code()) {
                    case 200:
                        GetWeight getWeight = response.body();
                        assert getWeight != null;
                        String[] days = new String[getWeight.getWeights().size()];
                        List<Entry> entries = new ArrayList<>();
                        for (int i = 0; i < getWeight.getWeights().size(); i++) {
                            entries.add(new Entry((float) i + 1, getWeight.getWeights().get(i).getWeight().floatValue()));
                            days[i] = ("Day " + (i));
                        }

                        lineChart.getDescription().setEnabled(false);

                        //Set styling data
                        LineDataSet dataSet = new LineDataSet(entries, "");
                        dataSet.setCircleRadius(5);
                        dataSet.setColor(Color.GRAY);
                        dataSet.setCircleColorHole(Color.RED);
                        dataSet.setCircleColor(Color.RED);
                        dataSet.setDrawValues(false);

                        //Set styling x axis
                        XAxis xAxis = lineChart.getXAxis();
                        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                        xAxis.setDrawGridLines(false);
                        xAxis.setTextSize(11);
                        xAxis.setTextColor(Color.rgb(74, 74, 74));
                        xAxis.setValueFormatter(new IndexAxisValueFormatter(days));
                        xAxis.setGranularity(1);

                        //Set styling y axys (left)
                        YAxis leftAxis = lineChart.getAxisLeft();
                        leftAxis.setDrawGridLines(false);
                        leftAxis.setTextColor(Color.rgb(74,74,74));
                        leftAxis.setTextSize(11);

                        //Set styling y axys (right)
                        YAxis rightAxis = lineChart.getAxisRight();
                        rightAxis.setDrawGridLines(false);
                        rightAxis.setDrawLabels(false);

                        Legend legend = lineChart.getLegend();
                        legend.setEnabled(false);

                        //Set chart instance
                        LineData lineData = new LineData(dataSet);

                        lineChart.setData(lineData);

                        //Refresh chart
                        lineChart.invalidate();
                        break;
                    case 400:
                        break;
                    case 500:
                        break;
                    default:
                        break;
                }
                dialogWeight.dismiss();
            }

            @Override
            public void onFailure(Call<GetWeight> call, Throwable t) {
                dialogWeight.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }

    private void setWeight() {
        // Create the call of the service view program progress
        Call<ResponseBody> call = services.set_weight(user.getId(), weightValue);

        // Executing call of the service time to start
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        createToast();
                        loadWeight();
                        dialog.dismiss();
                        getPermissions();
                        break;
                    default:
                        dialogWeight.dismiss();
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }

    private void loadProgramProgress(){
        dialogDays = loader();
        dialogDays.show();
        // Create the call of the service view program progress
        Call<ViewProgramProgress> call = services.view_program_progress(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ViewProgramProgress>() {
            @Override
            public void onResponse(Call<ViewProgramProgress> call, Response<ViewProgramProgress> response) {
                switch (response.code()){
                    case 200:
                        ViewProgramProgress data = response.body();
                        if(data.getIs_active()){
                            lblProgress.setText(data.getProgress_program());
                        }else{
                            session.CloseSession();
                            Intent i = new Intent(getContext(), Login.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            getActivity().finish();
                        }
                        break;
                    default:

                        break;
                }
                dialogDays.dismiss();
            }

            @Override
            public void onFailure(Call<ViewProgramProgress> call, Throwable t) {
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }

    private void getPermissions(){
        dialogPermission = loader();
        dialogPermission.show();
        // Create the call of the service view program progress
        Call<DoDailyCheck> call = services.do_daily_check(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<DoDailyCheck>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<DoDailyCheck> call, Response<DoDailyCheck> response) {
                switch (response.code()){
                    case 200:
                        DoDailyCheck data = response.body();
                        assert data != null;
                        if(!data.getWeigh_in()){
                           setWeightBtn.setEnabled(false);
                            setWeightBtn.setText("You've already logged your weight today");
                            setWeightBtn.setAllCaps(false);
                            setWeightBtn.setTextSize(10);
                            setWeightBtn.setBackground(getResources().getDrawable(R.drawable.gray_disabled_rounded_button));
                        }

                        break;
                    default:

                        break;
                }
                dialogPermission.dismiss();
            }

            @Override
            public void onFailure(Call<DoDailyCheck> call, Throwable t) {
                dialogPermission.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }

    private void createToast(){
        Toast toast = Toast.makeText(getView().getContext(), "Success", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext())
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

    private MaterialDialog.Builder createMessage(){
        return new MaterialDialog.Builder(getContext())
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }
}
