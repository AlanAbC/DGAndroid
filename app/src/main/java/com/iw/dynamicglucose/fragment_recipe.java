package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.get_single_recipe.GetSingleRecipe;
import com.iw.dynamicglucose.services.get_single_recipe.Ingredient;
import com.iw.dynamicglucose.services.get_single_recipe.Step;
import com.iw.dynamicglucose.services.view_progress_program.ViewProgramProgress;
import com.squareup.picasso.Picasso;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class fragment_recipe extends Fragment {

    // Declare vars layout
    private TextView txtDays;
    private TextView titleRecipe;
    private TextView timeRecipe;
    private TextView numberServing;
    private TextView ingredients;
    private TextView steps;
    private TextView txtRate;
    private TextView txt_description_content;
    private TextView txt_nutritional_content;
    private ImageView imageRecipe;
    private ImageButton rate1;
    private ImageButton rate2;
    private ImageButton rate3;
    private ImageButton rate4;
    private ImageButton rate5;
    private Button markDone;
    private ImageView back_button_meals;
    private BottomNavigationView bottomNavigationView;
    private Services services;
    private Session session;
    private UserSession user;
    // Declare other vars
    private String recipe_id;
    private GetSingleRecipe recipe;
    private MaterialDialog dialog;
    private Boolean come_from_home = false;

    // TODO: Rename parameter arguments, choose names that match
    public static fragment_recipe newInstance(Bundle arguments){
        fragment_recipe fragment = new fragment_recipe();
        if(arguments != null){
            fragment.setArguments(arguments);
        }
        return fragment;
    }

    public static fragment_recipe newInstance() {
        return new fragment_recipe();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment_recipe, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Assignment vars config
        Service service = new Service();
        services = service.getService();
        session = new Session(getContext());
        user = session.getSession();
        // Assignment vars layout
        txtDays = getView().findViewById(R.id.lbl_days);
        titleRecipe = getView().findViewById(R.id.txt_title_recipe);
        timeRecipe = getView().findViewById(R.id.txt_time_prep);
        numberServing = getView().findViewById(R.id.txt_no_servings);
        ingredients = getView().findViewById(R.id.txt_ingredients_content);
        steps = getView().findViewById(R.id.txt_directions_content);
        txtRate = getView().findViewById(R.id.txt_rate);
        imageRecipe = getView().findViewById(R.id.img_recipe);
        txt_description_content = getView().findViewById(R.id.txt_description_content);
        txt_nutritional_content = getView().findViewById(R.id.txt_nutritional_content);
        rate1 = getView().findViewById(R.id.btn_rate_1);
        rate2 = getView().findViewById(R.id.btn_rate_2);
        rate3 = getView().findViewById(R.id.btn_rate_3);
        rate4 = getView().findViewById(R.id.btn_rate_4);
        rate5 = getView().findViewById(R.id.btn_rate_5);
        markDone = getView().findViewById(R.id.btn_markDone);
        back_button_meals = getView().findViewById(R.id.back_button_meals);
        bottomNavigationView = ((Home)getActivity()).findViewById(R.id.navigation);

        recipe_id = getArguments().getString("id");
        come_from_home = getArguments().getBoolean("come_from_home", false);

        setListeners();

        loadProgramProgress();
        loadRecipe();

    }

    private void setListeners() {
        markDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                markAsDone();
            }
        });
        rate1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(1);
            }
        });
        rate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(2);
            }
        });
        rate3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(3);
            }
        });
        rate4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(4);
            }
        });
        rate5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(5);
            }
        });
        back_button_meals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(come_from_home){
                    MenuItem item = bottomNavigationView.getMenu().getItem(0);
                    item.setChecked(true);
                    fragment_home fragment = fragment_home.newInstance();
                    FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame_layout, fragment);
                    ft.commit();
                }else{
                    MenuItem item = bottomNavigationView.getMenu().getItem(1);
                    item.setChecked(true);
                    fragment_meals fragment = fragment_meals.newInstance();
                    FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame_layout, fragment);
                    ft.commit();
                }

            }
        });
    }

    private void loadProgramProgress() {
        // Create the call of the service view program progress
        Call<ViewProgramProgress> call = services.view_program_progress(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ViewProgramProgress>() {
            @Override
            public void onResponse(Call<ViewProgramProgress> call, Response<ViewProgramProgress> response) {
                switch (response.code()){
                    case 200:
                        ViewProgramProgress data = response.body();
                        assert data != null;
                        if(data.getIs_active()){
                            txtDays.setText(data.getProgress_program());
                        }else{
                            session.CloseSession();
                            Intent i = new Intent(getContext(), Login.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            getActivity().finish();
                        }

                        break;
                    default:

                        break;
                }
            }

            @Override
            public void onFailure(Call<ViewProgramProgress> call, Throwable t) {
                createToast("Error: " + t.getMessage());
            }
        });
    }

    private void loadRecipe() {
        dialog = loader();
        dialog.show();
        // Create the call of the service view program progress
        Call<GetSingleRecipe> call = services.get_single_recipe(recipe_id, user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<GetSingleRecipe>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<GetSingleRecipe> call, Response<GetSingleRecipe> response) {
                switch(response.code()){
                    case 200:
                        GetSingleRecipe data = response.body();
                        recipe = data;
                        // Mark as done
                        assert data != null;
                        if(data.getDone()){
                            markDone.setBackground(getResources().getDrawable(R.drawable.yellow_selected_rounded_button));
                            markDone.setTextColor(Color.WHITE);
                            markDone.setText("DONE");
                            markDone.setEnabled(false);
                        }
                        // Rate
                        setRate(data.getRating());
                        txt_description_content.setText(data.getDescription());
                        titleRecipe.setText(data.getName());
                        timeRecipe.setText(data.getTimePrep());
                        numberServing.setText("No. of Servings: " + data.getNoServing());
                        StringBuilder flagIngredients = new StringBuilder();
                        StringBuilder flagSteps = new StringBuilder();
                        StringBuilder flagNutrtional = new StringBuilder();
                        for (Ingredient ingredients : data.getIngredients()){
                            flagIngredients.append(ingredients.getItem()).append("\n");
                        }
                        ingredients.setText(flagIngredients.toString());
                        for (int i = 0; i < data.getSteps().size(); i++){
                            Step step = data.getSteps().get(i);
                            flagSteps.append(step.getStep()).append("\n");
                        }
                        steps.setText(flagSteps.toString());
                        for (int i = 0; i < data.getNutritionalFacts().size(); i++){
                            flagNutrtional.append(data.getNutritionalFacts().get(i)).append("\n");
                        }
                        txt_nutritional_content.setText(flagNutrtional.toString());
                        txtRate.setText(data.getRatingAverage() + "/5.0");
                        if(data.getPhoto() != null){
                            if(data.getPhoto() != ""){
                                Picasso.with(getContext()).load(data.getPhoto()).into(imageRecipe);
                            }
                        }
                        break;

                    default:
                        break;
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<GetSingleRecipe> call, Throwable t) {
                dialog.dismiss();
                createToast("Error: " + t.getMessage());
            }
        });
    }

    private void setRate(Integer rating) {
        if(rating != 0){
            rate1.setEnabled(false);
            rate2.setEnabled(false);
            rate3.setEnabled(false);
            rate4.setEnabled(false);
            rate5.setEnabled(false);
        }
        switch (rating){
            case 0:{
                break;
            }
            case 1:{
                rate1.setImageResource(R.drawable.star_checked);
                break;
            }
            case 2:{
                rate1.setImageResource(R.drawable.star_checked);
                rate2.setImageResource(R.drawable.star_checked);
                break;
            }
            case 3:{
                rate1.setImageResource(R.drawable.star_checked);
                rate2.setImageResource(R.drawable.star_checked);
                rate3.setImageResource(R.drawable.star_checked);
                break;
            }
            case 4:{
                rate1.setImageResource(R.drawable.star_checked);
                rate2.setImageResource(R.drawable.star_checked);
                rate3.setImageResource(R.drawable.star_checked);
                rate4.setImageResource(R.drawable.star_checked);
                break;
            }
            case 5:{
                rate1.setImageResource(R.drawable.star_checked);
                rate2.setImageResource(R.drawable.star_checked);
                rate3.setImageResource(R.drawable.star_checked);
                rate4.setImageResource(R.drawable.star_checked);
                rate5.setImageResource(R.drawable.star_checked);
                break;
            }
            default:{
                break;
            }
        }
    }

    private void markAsDone(){
        dialog = loader();
        dialog.show();
        // Create the call of the service time to start
        Call<ResponseBody> call = services.register_progress_day_food(2, recipe.getMealTime(), user.getId(), Integer.parseInt(recipe_id));

        // Executing call of the service time to start
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        markDone.setBackground(getResources().getDrawable(R.drawable.yellow_selected_rounded_button));
                        markDone.setTextColor(Color.WHITE);
                        markDone.setText("DONE");
                        markDone.setEnabled(false);
                        break;
                    default:
                        break;
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                createToast("Error: " + t.getMessage());
            }
        });
    }

    private void rate(final Integer rating){
        dialog = loader();
        dialog.show();
        // Create the call of the service time to start
        Call<ResponseBody> call = services.rating_recipe(user.getId(), Integer.parseInt(recipe_id), rating);

        // Executing call of the service time to start
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        setRate(rating);
                        break;
                    default:
                        break;
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                createToast("Error: " + t.getMessage());
            }
        });
    }

    private void createToast(String msg){
        Toast toast = Toast.makeText(getContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext())
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }
}
