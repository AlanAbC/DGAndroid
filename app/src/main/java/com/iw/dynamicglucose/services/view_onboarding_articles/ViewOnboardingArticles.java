
package com.iw.dynamicglucose.services.view_onboarding_articles;

import java.util.List;

public class ViewOnboardingArticles {

    private List<Article> articles = null;
    private Boolean disableReason;
    private Boolean showReason;

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public Boolean getDisableReason() {
        return disableReason;
    }

    public void setDisableReason(Boolean disableReason) {
        this.disableReason = disableReason;
    }

    public Boolean getShowReason() {
        return showReason;
    }

    public void setShowReason(Boolean showReason) {
        this.showReason = showReason;
    }

}
