package com.iw.dynamicglucose;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.BaseUrl;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.scottyab.aescrypt.AESCrypt;

import java.security.GeneralSecurityException;
import javax.crypto.spec.SecretKeySpec;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Login extends AppCompatActivity {

    // Declare vars layout
    private ImageButton login;
    private ImageButton backButton;
    private EditText email;
    private EditText password;
    private TextView tapHere;
    private TextView textDG;

    // Declare vars config
    private BaseUrl baseUrl;
    private Session session;
    private UserSession user;

    // Declare var progress dialog
    private  MaterialDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // Assignment vars layout
        login = findViewById(R.id.loginButton);
        backButton = findViewById(R.id.backButton);
        email = findViewById(R.id.input_usr);
        password = findViewById(R.id.input_pass);
        tapHere = findViewById(R.id.tap_here);
        textDG = findViewById(R.id.textDG);
        // Assignment vars config
        baseUrl = new BaseUrl();
        session = new Session(getApplicationContext());
        user = session.getSession();

        if(session.isFirstTime()){
            startActivity(new Intent(Login.this, WelcomeSlider.class));
            finish();
        }

        validateSession();
        setListeners();
    }


    private void setListeners() {
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    validateForm();
                } catch (Exception ignored) {
                }

            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), WelcomeSlider.class);
                startActivity(i);
            }
        });

        tapHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), PasswordReset.class);
                startActivity(i);
            }
        });
        textDG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse("https://dynamicglucose.com/"));
                startActivity(viewIntent);
            }
        });
    }

    private void validateForm() throws Exception {
        if (!email.getText().toString().equals("") && !password.getText().toString().equals("")) {

            String txt = encrypt(password.getText().toString());


            dialog = loader();
            dialog.show();
            // Create object retrofit for petition
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl.getUrl())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            // Create service from te interface of services
            Services services = retrofit.create(Services.class);

            // Create the call of the service time to start
            Call<UserSession> call = services.login(email.getText().toString(), txt);

            // Executing call of the service time to start
            call.enqueue(new Callback<UserSession>() {
                @Override
                public void onResponse(Call<UserSession> call, Response<UserSession> response) {
                    switch (response.code()) {
                        case 200:
                            UserSession data = response.body();
                            assert data != null;
                            if(data.getCode() == 200){
                                data.setShow_mindsets(true);
                                session.setstateNotif(true);
                                session.setUser(data);
                                Integer createSession = session.CreateSession();
                                if(createSession == 1){
                                    Intent i = new Intent(getApplicationContext(), SplashScreen.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    startActivity(i);
                                    finish();
                                }else{
                                    createToast("Error \n Error to create session");
                                }
                            }else{
                                createToast("Error \n" + data.getMessage());
                            }
                            break;
                        default:
                            break;
                    }
                    dialog.dismiss();
                }

                @Override
                public void onFailure(Call<UserSession> call, Throwable t) {
                    dialog.dismiss();
                    Log.e("errorCall", t.getMessage());
                }
            });

        }else{
            createToast("Error \n Please enter all fields");
        }
    }
    public static String encrypt(String plainText) throws GeneralSecurityException {
        String secretKey64 = "41386433326530382e41386433326530";
        String iv64 = "gqLOHUioQ0QjhuvI";
        byte[] decodedKey = secretKey64.getBytes();
        int base64Mode = android.util.Base64.NO_WRAP;
        String encrytedText = "";
        if (plainText == null) {
            return null;
        } else {
            SecretKeySpec secretKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
            byte[] iv = iv64.getBytes();
            byte[] txt = plainText.getBytes();

            byte[] dataBytes = AESCrypt.encrypt(secretKey, iv, txt);
            encrytedText = Base64.encodeToString(dataBytes, base64Mode);
        }

        return encrytedText;
    }

    private void validateSession(){
        if(user.getId() > 0){
            Intent i = new Intent(getApplicationContext(), Home.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
            finish();
        }
    }

    private void createToast(String msg){
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }
}
