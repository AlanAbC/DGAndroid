package com.iw.dynamicglucose;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.halilibo.bettervideoplayer.BetterVideoCallback;
import com.halilibo.bettervideoplayer.BetterVideoPlayer;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LifestyleVideo extends AppCompatActivity implements BetterVideoCallback {
    private Services services;
    private UserSession user;
    private TextView question;
    private VideoView video;
    private Boolean flagSettings = false;
    private EditText answer;
    private EditText answer2;
    private RelativeLayout skip;
    private BetterVideoPlayer player;
    //loader
    private MaterialDialog dialogLifestyle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lifestyle_video);
        getSupportActionBar().hide();

        flagSettings = getIntent().getBooleanExtra("settings", false);

        final Service service = new Service();
        services = service.getService();
        Session session = new Session(getBaseContext());
        user = session.getSession();
        question = findViewById(R.id.txt_title_lifestyle);
        Button button = findViewById(R.id.btn_lifestyle);
        //video = findViewById(R.id.video_lifestyle);
        answer = findViewById(R.id.input_videoAnswer);
        answer2 = findViewById(R.id.input_videoAnswer2);
        player = findViewById(R.id.player);
        player.setCallback(this);
        player.setAutoPlay(true);
        skip = findViewById(R.id.skip);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(answer.getText().toString().equals("") || answer2.getText().toString().equals("")){
                    createToast("Please enter the two answers");
                }else{
                    setLifstyle(answer.getText().toString(), answer2.getText().toString());
                }
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<ResponseBody> call = services.skip_video(user.getId());
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(flagSettings){
                            finish();
                        }else{
                            Intent i = new Intent(getApplicationContext(), PrevFoodSelection.class);
                            createToast("Success");
                            startActivity(i);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }
        });
        loadLifestyle();
    }

    private void loadLifestyle(){
        dialogLifestyle = loader();
        dialogLifestyle.show();
        // Create the call of the service view program progress
        Call<com.iw.dynamicglucose.services.lifestyle_video.LifestyleVideo> call = services.get_lifestyle_video();

        // Executing call of the service view program progress
        call.enqueue(new Callback<com.iw.dynamicglucose.services.lifestyle_video.LifestyleVideo>() {
            @Override
            public void onResponse(Call<com.iw.dynamicglucose.services.lifestyle_video.LifestyleVideo> call, Response<com.iw.dynamicglucose.services.lifestyle_video.LifestyleVideo> response) {
                switch (response.code()) {
                    case 200:
                        final com.iw.dynamicglucose.services.lifestyle_video.LifestyleVideo data = response.body();

                        assert data != null;
                        //video.setVideoURI(Uri.parse(data.getFile()));
                        //video.start();
                        player.setSource(Uri.parse(data.getFile()));
                        question.setText(data.getQuestion());
                        break;
                    default:

                        break;
                }
                dialogLifestyle.dismiss();
            }

            @Override
            public void onFailure(Call<com.iw.dynamicglucose.services.lifestyle_video.LifestyleVideo> call, Throwable t) {
                dialogLifestyle.dismiss();
                createToast("Error: " + t.getMessage());
            }
        });
    }
    private void  setLifstyle(String info, String info2){
        dialogLifestyle = loader();
        dialogLifestyle.show();
        // Create the call of the service view program progress
        Call<ResponseBody> call = services.set_question_file(user.getId(), info, info2);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        if(flagSettings){
                            finish();
                        }else{
                            Intent i = new Intent(getApplicationContext(), PrevFoodSelection.class);
                            startActivity(i);
                            finish();
                        }
                        break;
                    default:
                        createToast("Server not avaliable, try again later");
                        break;
                }
                dialogLifestyle.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                createToast("Error" + t);
            }
        });
    }
    private void createToast(String msg){
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

    @Override
    protected void onPause() {
        super.onPause();
        player.pause();
    }

    @Override
    public void onStarted(BetterVideoPlayer player) {

    }

    @Override
    public void onPaused(BetterVideoPlayer player) {

    }

    @Override
    public void onPreparing(BetterVideoPlayer player) {

    }

    @Override
    public void onPrepared(BetterVideoPlayer player) {

    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(BetterVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(BetterVideoPlayer player) {

    }

    @Override
    public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {

    }
}
