package com.iw.dynamicglucose.services.view_progress_program;

/**
 * Created by IW on 01/feb/2018.
 */

public class ViewProgramProgress {

    private String progress_program;
    private Boolean is_active;

    public String getProgress_program() {
        return progress_program;
    }

    public void setProgress_program(String progress_program) {
        this.progress_program = progress_program;
    }

    public Boolean getIs_active() {
        return is_active;
    }

    public void setIs_active(Boolean is_active) {
        this.is_active = is_active;
    }
}
