package com.iw.dynamicglucose.ExtraObjects;

import java.util.ArrayList;

/**
 * Created by IW on 14/02/2018.
 */

public class DataLifestyle {
    private Integer patient_id;
    private Integer screen;
    private ArrayList<Long> array;

    public DataLifestyle(Integer patient_id, Integer screen, ArrayList<Long> answer_id) {
        this.patient_id = patient_id;
        this.screen = screen;
        this.array = answer_id;
    }

}
