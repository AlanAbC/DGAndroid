package com.iw.dynamicglucose.services.applogin;

/**
 * Created by IW on 01/feb/2018.
 */

public class UserSession {

    private Integer dg_program;
    private String email;
    private Integer message_id;
    private String token;
    private String avatar;
    private Integer id;
    private Integer code;
    private Boolean show_mindsets;
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getDg_program() {
        return dg_program;
    }

    public void setDg_program(Integer dg_program) {
        this.dg_program = dg_program;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getMessage_id() {
        return message_id;
    }

    public void setMessage_id(Integer message_id) {
        this.message_id = message_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getShow_mindsets() {
        return show_mindsets;
    }

    public void setShow_mindsets(Boolean show_mindsets) {
        this.show_mindsets = show_mindsets;
    }
}
