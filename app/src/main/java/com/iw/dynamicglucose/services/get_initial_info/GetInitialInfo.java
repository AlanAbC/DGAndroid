package com.iw.dynamicglucose.services.get_initial_info;

/**
 * Created by IW on 06/feb/2018.
 */

public class GetInitialInfo {

    private String timeToStart;
    private Boolean onboardFinish;
    private Boolean programFinish;
    private Boolean goal;
    private String position;

    public String getTimeToStart() {
        return timeToStart;
    }

    public void setTimeToStart(String timeToStart) {
        this.timeToStart = timeToStart;
    }

    public Boolean getOnboardFinish() {
        return onboardFinish;
    }

    public void setOnboardFinish(Boolean onboardFinish) {
        this.onboardFinish = onboardFinish;
    }

    public Boolean getProgramFinish() {
        return programFinish;
    }

    public void setProgramFinish(Boolean programFinish) {
        this.programFinish = programFinish;
    }

    public Boolean getGoal() {
        return goal;
    }

    public void setGoal(Boolean goal) {
        this.goal = goal;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

}
