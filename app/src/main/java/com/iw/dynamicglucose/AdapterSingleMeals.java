package com.iw.dynamicglucose;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iw.dynamicglucose.services.single_meals.Item;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by IW on 09/02/2018.
 */

public class AdapterSingleMeals extends RecyclerView.Adapter<AdapterSingleMeals.ViewHolder> {
    private List<Item> list;
    private Context context;

    AdapterSingleMeals(Context context, List<Item> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public AdapterSingleMeals.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AdapterSingleMeals.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_single_meals, parent, false));
    }

    @Override
    public void onBindViewHolder(AdapterSingleMeals.ViewHolder holder, int position) {
        Item item = list.get(position);
        holder.titleMeal.setText(item.getName());
        Picasso.with(context).load(item.getPhoto()).into(holder.imageMeal);
        if (position == (list.size()-1)){
            holder.plusImage.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imageMeal;
        TextView titleMeal;
        ImageView plusImage;

        public ViewHolder(View itemView) {
            super(itemView);
            imageMeal = itemView.findViewById(R.id.img_cell_single_meals);
            plusImage = itemView.findViewById(R.id.plus_cell_single_meals);
            titleMeal = itemView.findViewById(R.id.txt_cell_single_meals);

        }
    }
}
