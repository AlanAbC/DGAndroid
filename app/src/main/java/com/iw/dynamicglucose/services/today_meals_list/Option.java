
package com.iw.dynamicglucose.services.today_meals_list;

import java.util.List;

public class Option {

    private Integer group;
    private List<String> name = null;
    private List<Recipe> recipe = null;

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public List<String> getName() {
        return name;
    }

    public void setName(List<String> name) {
        this.name = name;
    }

    public List<Recipe> getRecipe() {
        return recipe;
    }

    public void setRecipe(List<Recipe> recipe) {
        this.recipe = recipe;
    }

}
