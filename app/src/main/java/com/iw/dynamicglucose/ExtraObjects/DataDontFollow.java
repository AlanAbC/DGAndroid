package com.iw.dynamicglucose.ExtraObjects;

/**
 * Created by IW on 20/02/2018.
 */

public class DataDontFollow {

    private Integer patient_id;
    private String text;

    public DataDontFollow(Integer patient_id, String text) {
        this.patient_id = patient_id;
        this.text = text;
    }
}
