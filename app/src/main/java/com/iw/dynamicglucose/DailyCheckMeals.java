package com.iw.dynamicglucose;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.ExtraObjects.DataDailyMeals;
import com.iw.dynamicglucose.ExtraObjects.OnItemClick;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.DailyWorkout.DailyWorkouts;
import com.iw.dynamicglucose.services.DailyWorkout.Workout;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.get_daily_check_meals.GetDailyCheckMeal;
import com.iw.dynamicglucose.services.get_daily_check_meals.Option;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DailyCheckMeals extends AppCompatActivity {
    private LinearLayout Content;

    private Services services;
    private UserSession user;
    private  MaterialDialog dialog;

    private ArrayList<ArrayList<Boolean>> flags = new ArrayList<>();
    private ArrayList<ArrayList<Integer>> ids = new ArrayList<>();
    private ArrayList<Integer> mealTimes = new ArrayList<>();
    private ArrayList<Boolean> flagsDontFollow = new ArrayList<>();
    //loader
    private MaterialDialog dialogGetInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_check_meals);
        getSupportActionBar().hide();
        // Assignment vars config
        Service service = new Service();
        services = service.getService();
        Session session = new Session(getApplicationContext());
        user = session.getSession();

        //Assignment vars layout
        ImageButton closeDailyMeals = findViewById(R.id.close_daily_check);
        Content = findViewById(R.id.container_layout);
        Button nextButton = findViewById(R.id.btn_daily_meals);
        closeDailyMeals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataDailyMeals data = new DataDailyMeals();
                data.setPatient_id(user.getId());
                ArrayList<Map<String, Integer>> tempgroups = new ArrayList<>();
                for(int i = 0; i<flags.size(); i++){
                    if(!flagsDontFollow.get(i)){
                        for(int j = 0; j< flags.get(i).size(); j++){
                            if(flags.get(i).get(j)){
                                Map<String, Integer> map = new HashMap<>();
                                map.put("meal_time", mealTimes.get(i));
                                map.put("group", ids.get(i).get(j));
                                tempgroups.add(map);
                            }
                        }
                    }
                }
                data.setGroups(tempgroups);
                setDailyCheck(data);
            }
        });
        loadDailyCheck();
    }
    private void loadWorkoutse(){
        // Create the call of the service view program progress
        Call<DailyWorkouts> call = services.get_daily_workouts(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<DailyWorkouts>() {
            @Override
            public void onResponse(Call<DailyWorkouts> call, Response<DailyWorkouts> response) {
                switch (response.code()) {
                    case 200:
                        if(flagsDontFollow.contains(true)){
                            Intent i = new Intent(getApplicationContext(), DontFollowPlan.class);
                            startActivity(i);
                        }else{
                            Intent i = new Intent(getApplicationContext(), DailyCheckWorkouts.class);
                            startActivity(i);
                        }
                        break;
                    case 204:
                        if(flagsDontFollow.contains(true)){
                            Intent i = new Intent(getApplicationContext(), DontFollowPlan.class);
                            startActivity(i);
                            finish();
                        }else{
                            Intent i = new Intent(getApplicationContext(), DailyCheckoutGral.class);
                            startActivity(i);
                        }
                        break;
                    default:

                        break;
                }
            }

            @Override
            public void onFailure(Call<DailyWorkouts> call, Throwable t) {
                createToast("Error: " + t.getMessage());
            }
        });
    }
    private void loadDailyCheck() {
        dialogGetInfo = loader();
        dialogGetInfo.show();
        // Create the call of the service view program progress
        Call<ArrayList<GetDailyCheckMeal>> call = services.get_daily_check_meals(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ArrayList<GetDailyCheckMeal>>() {
            @Override
            public void onResponse(Call<ArrayList<GetDailyCheckMeal>> call, Response<ArrayList<GetDailyCheckMeal>> response) {
                switch (response.code()){
                    case 200:
                        ArrayList<GetDailyCheckMeal> data = response.body();
                        assert data != null;
                        fillData(data);
                        break;

                    case 204:
                       // showListMeals.setVisibility(View.GONE);
                       // hiddenListMeals.setVisibility(View.VISIBLE);
                        break;

                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialogGetInfo.dismiss();
            }

            @Override
            public void onFailure(Call<ArrayList<GetDailyCheckMeal>> call, Throwable t) {
                dialogGetInfo.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });

    }
    //Funcion que realiza la paticion al servicio para actualizar el perfil
    private void  setDailyCheck(DataDailyMeals info){
        dialogGetInfo = loader();
        dialogGetInfo.show();
        // Create the call of the service view program progress
        Call<ResponseBody> call = services.set_daily_meals(info);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        loadWorkoutse();
                        break;
                    default:
                        break;
                }
                dialogGetInfo.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                createToast("Error" + t);
                dialogGetInfo.dismiss();
            }
        });
    }
    private void fillData(ArrayList<GetDailyCheckMeal> data) {

        for(int i = 0; i < data.size(); i++) {
            ArrayList<Boolean> tempFlag = new ArrayList<>();
            ArrayList<Integer> tempIds = new ArrayList<>();
            mealTimes.add(data.get(i).getMealTimeId());
            flagsDontFollow.add(false);
            for(Option opt: data.get(i).getOptions()){
                tempFlag.add(opt.getDone());
                tempIds.add(opt.getGroup());
            }
            flags.add(tempFlag);
            ids.add(tempIds);
            final GetDailyCheckMeal dailyCheckMeal = data.get(i);
            RelativeLayout header = new RelativeLayout(getApplicationContext());
            getLayoutInflater().inflate(R.layout.cell_header_meals, header);

            final RelativeLayout didNot = new RelativeLayout(getApplicationContext());
            getLayoutInflater().inflate(R.layout.didnt_follow, didNot);
            final int finalI = i;

            ImageView icon_header = header.findViewById(R.id.img_cell_header_meals);
            TextView title_header = header.findViewById(R.id.txt_title_cell_header_meals);
            TextView time_header = header.findViewById(R.id.txt_time_cell_header_meals);

            Picasso.with(getApplicationContext()).load(dailyCheckMeal.getImage()).into(icon_header);
            title_header.setText(dailyCheckMeal.getMealTimeName());
            time_header.setText(dailyCheckMeal.getMealTime());

            LinearLayout content = new LinearLayout(getApplicationContext());
            content.setOrientation(LinearLayout.HORIZONTAL);

            ScrollView scroll = new ScrollView(getApplicationContext());
            scroll.setFillViewport(true);
            final RecyclerView dailyMeal = new RecyclerView(getApplicationContext());
            RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
            dailyMeal.setLayoutParams(params);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
            dailyMeal.setLayoutManager(layoutManager);
            AdapterDailyMeals adapter = new AdapterDailyMeals(getApplicationContext(), dailyCheckMeal.getOptions(), i, new OnItemClick() {
                @Override
                public void onItemClick(Option item, Integer position, Integer rPosition) {
                    if(flags.get(rPosition).get(position)){
                        flags.get(rPosition).set(position, false);
                    }else{
                        flags.get(rPosition).set(position, true);
                    }
                }
            });
            dailyMeal.setAdapter(adapter);
            didNot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (flagsDontFollow.get(finalI)){
                        AdapterDailyMeals adapterDailyMeals = (AdapterDailyMeals) dailyMeal.getAdapter();
                        adapterDailyMeals.setClickable(true);
                        dailyMeal.setAdapter(adapterDailyMeals);
                        flagsDontFollow.set(finalI, false);
                        didNot.setBackgroundColor(Color.parseColor("#F7F7F7"));
                        didNot.findViewById(R.id.container_didnt_follow).setBackgroundColor(Color.parseColor("#F7F7F7"));
                        TextView text = didNot.findViewById(R.id.txt_didnt_follow);
                        text.setTextColor(Color.parseColor("#ED1C24"));
                    }else{
                        AdapterDailyMeals adapterDailyMeals = (AdapterDailyMeals) dailyMeal.getAdapter();
                        adapterDailyMeals.setClickable(false);
                        dailyMeal.setAdapter(adapterDailyMeals);
                        flagsDontFollow.set(finalI, true);
                        didNot.setBackgroundColor(Color.parseColor("#60D586"));
                        didNot.findViewById(R.id.container_didnt_follow).setBackgroundColor(Color.parseColor("#60D586"));
                        TextView text = didNot.findViewById(R.id.txt_didnt_follow);
                        text.setTextColor(Color.parseColor("#FFFFFF"));
                    }

                }
            });
            scroll.addView(content);
            Content.addView(header);
            Content.addView(dailyMeal);
            Content.addView(didNot);
        }
    }


    private void createToast(String msg){
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }
    private MaterialDialog.Builder createMessage(){
        return new MaterialDialog.Builder(this)
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }

}
