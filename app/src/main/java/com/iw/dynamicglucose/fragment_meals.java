package com.iw.dynamicglucose;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.today_meals_list.Option;
import com.iw.dynamicglucose.services.today_meals_list.Recipe;
import com.iw.dynamicglucose.services.today_meals_list.TodayMealsList;
import com.iw.dynamicglucose.services.view_progress_program.ViewProgramProgress;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class fragment_meals extends Fragment {

    // Declare vars layout
    private TextView txtDays;
    private LinearLayout list_meals;
    private RelativeLayout showListMeals;
    private RelativeLayout hiddenListMeals;

    private Services services;
    private Session session;
    private UserSession user;

    private  MaterialDialog dialog;

    public static fragment_meals newInstance() {
        return new fragment_meals();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment_meals, container, false);
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        // Assignment vars config
        Service service = new Service();
        services = service.getService();
        session = new Session(getContext());
        user = session.getSession();

        // Assignment vars layout
        txtDays = getView().findViewById(R.id.lbl_days);
        list_meals = getView().findViewById(R.id.list_meals);
        showListMeals = getView().findViewById(R.id.show_list_meals);
        hiddenListMeals = getView().findViewById(R.id.hidden_list_meals);

        if(session.getCodeNotif() != 0){
            session.setCodeNotif(0);
        }

        loadProgramProgress();
        loadMealsList();
    }

    private void loadProgramProgress(){
        dialog = loader();
        dialog.show();
        // Create the call of the service view program progress
        Call<ViewProgramProgress> call = services.view_program_progress(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ViewProgramProgress>() {
            @Override
            public void onResponse(Call<ViewProgramProgress> call, Response<ViewProgramProgress> response) {
                switch (response.code()){
                    case 200:
                        ViewProgramProgress data = response.body();
                        assert data != null;
                        if(data.getIs_active()){
                            txtDays.setText(data.getProgress_program());
                        }else{
                            session.CloseSession();
                            Intent i = new Intent(getContext(), Login.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            getActivity().finish();
                        }

                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<ViewProgramProgress> call, Throwable t) {
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }

    private void loadMealsList(){
        // Create the call of the service view program progress
        Call<ArrayList<TodayMealsList>> call = services.today_meals_list(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ArrayList<TodayMealsList>>() {
            @Override
            public void onResponse(Call<ArrayList<TodayMealsList>> call, Response<ArrayList<TodayMealsList>> response) {
                switch (response.code()){
                    case 200:
                        ArrayList<TodayMealsList> data = response.body();
                        fillMealsList(data);
                        break;

                    case 204:
                        showListMeals.setVisibility(View.GONE);
                        hiddenListMeals.setVisibility(View.VISIBLE);
                        break;

                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ArrayList<TodayMealsList>> call, Throwable t) {
                dialog.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }

    private void fillMealsList(ArrayList<TodayMealsList> data) {
        for(int i = 0; i < data.size(); i++){
            TodayMealsList todayMealsList = data.get(i);
            RelativeLayout header = new RelativeLayout(getContext());
            getLayoutInflater().inflate(R.layout.cell_header_meals, header);

            ImageView icon_header = header.findViewById(R.id.img_cell_header_meals);
            TextView title_header = header.findViewById(R.id.txt_title_cell_header_meals);
            TextView time_header = header.findViewById(R.id.txt_time_cell_header_meals);

            Picasso.with(getContext()).load(todayMealsList.getImage()).into(icon_header);
            title_header.setText(todayMealsList.getMealTimeName());
            time_header.setText(todayMealsList.getMealTime());

            LinearLayout content = new LinearLayout(getContext());
            content.setOrientation(LinearLayout.VERTICAL);

            for(int j = 0; j < todayMealsList.getOptions().size(); j++){
                Option option = todayMealsList.getOptions().get(j);

                RelativeLayout and = new RelativeLayout(getContext());
                getLayoutInflater().inflate(R.layout.cell_and_meals, and);
                RelativeLayout or = new RelativeLayout(getContext());
                getLayoutInflater().inflate(R.layout.cell_or_meals, or);

                StringBuilder name_complete = new StringBuilder();

                if(option.getName() != null && option.getRecipe() != null){
                    for(int k = 0; k < option.getName().size(); k++){
                        String name = option.getName().get(k);
                        if(k != option.getName().size() - 1){
                            name_complete.append(name).append(", ");
                        }else{
                            if(option.getName().size() == 1){
                                name_complete = new StringBuilder(name);
                            }else{
                                name_complete.append("and ").append(name);
                            }
                        }
                    }
                    RelativeLayout meals_name = new RelativeLayout(getContext());
                    getLayoutInflater().inflate(R.layout.cell_meals, meals_name);
                    TextView meals_name_content = meals_name.findViewById(R.id.txt_content);
                    meals_name_content.setText(name_complete.toString());
                    // Here function onclick
                    setOnClickSingleMeals(todayMealsList.getMealTimeId(), option.getGroup() ,todayMealsList.getMealTime(), meals_name, todayMealsList.getMealTimeName());
                    content.addView(meals_name);
                    content.addView(and);


                    // set recipes in lineal layout
                    for(int k = 0; k < option.getRecipe().size(); k++){
                        Recipe recipe = option.getRecipe().get(k);
                        if(k != option.getRecipe().size() - 1){
                            RelativeLayout meals_recipe = new RelativeLayout(getContext());
                            getLayoutInflater().inflate(R.layout.cell_meals, meals_recipe);
                            TextView meals_recipe_txt = meals_recipe.findViewById(R.id.txt_content);
                            meals_recipe_txt.setText(recipe.getName());
                            // Here function onclick
                            setOnclickRecipe(recipe.getRecipeId(), meals_recipe);
                            content.addView(meals_recipe);
                            content.addView(and);
                        }else{
                            RelativeLayout meals_recipe = new RelativeLayout(getContext());
                            getLayoutInflater().inflate(R.layout.cell_meals, meals_recipe);
                            TextView meals_recipe_txt = meals_recipe.findViewById(R.id.txt_content);
                            meals_recipe_txt.setText(recipe.getName());
                            // Here function onclick
                            setOnclickRecipe(recipe.getRecipeId(), meals_recipe);
                            content.addView(meals_recipe);
                        }
                    }
                }else{
                    if(option.getName() != null){
                        for(int k = 0; k < option.getName().size(); k++){
                            String name = option.getName().get(k);
                            if(k != option.getName().size() - 1){
                                name_complete.append(name).append(", ");
                            }else{
                                if(option.getName().size() == 1){
                                    name_complete = new StringBuilder(name);
                                }else{
                                    name_complete.append("and ").append(name);
                                }
                            }
                        }
                        RelativeLayout meals_name = new RelativeLayout(getContext());
                        getLayoutInflater().inflate(R.layout.cell_meals, meals_name);
                        TextView meals_name_content = meals_name.findViewById(R.id.txt_content);
                        meals_name_content.setText(name_complete.toString());
                        // Here function onclick
                        setOnClickSingleMeals(todayMealsList.getMealTimeId(), option.getGroup() ,todayMealsList.getMealTime(), meals_name, todayMealsList.getMealTimeName());
                        content.addView(meals_name);
                    }else{
                        // set recipes in lineal layout
                        for(int k = 0; k < option.getRecipe().size(); k++){
                            Recipe recipe = option.getRecipe().get(k);
                            if(k != option.getRecipe().size() - 1){
                                RelativeLayout meals_recipe = new RelativeLayout(getContext());
                                getLayoutInflater().inflate(R.layout.cell_meals, meals_recipe);
                                TextView meals_recipe_txt = meals_recipe.findViewById(R.id.txt_content);
                                meals_recipe_txt.setText(recipe.getName());
                                // Here function onclick
                                setOnclickRecipe(recipe.getRecipeId(), meals_recipe);
                                content.addView(meals_recipe);
                                content.addView(and);
                            }else{
                                RelativeLayout meals_recipe = new RelativeLayout(getContext());
                                getLayoutInflater().inflate(R.layout.cell_meals, meals_recipe);
                                TextView meals_recipe_txt = meals_recipe.findViewById(R.id.txt_content);
                                meals_recipe_txt.setText(recipe.getName());
                                // Here function onclick
                                setOnclickRecipe(recipe.getRecipeId(), meals_recipe);
                                content.addView(meals_recipe);
                            }
                        }
                    }
                }
                if(j != todayMealsList.getOptions().size() - 1){
                    content.addView(or);
                }
            }
            list_meals.addView(header);
            list_meals.addView(content);
        }

    }

    private void setOnclickRecipe(final Integer recipeId, RelativeLayout meals_recipe) {
        meals_recipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle arguments = new Bundle();
                arguments.putString("id", recipeId.toString());
                fragment_recipe fragment = fragment_recipe.newInstance(arguments);
                FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment);
                ft.commit();
            }
        });
    }

    private void setOnClickSingleMeals(final Integer mealId, final Integer group, final String meal_time, RelativeLayout meals_name, final String meal_time_string){
        meals_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle arguments = new Bundle();
                arguments.putString("meal_id", mealId.toString());
                arguments.putString("meal_time_string", meal_time_string);
                arguments.putString("group", group.toString());
                arguments.putString("meal_time", meal_time);
                Single_Meals fragment = Single_Meals.newInstance(arguments);
                FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment);
                ft.commit();
            }
        });
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext())
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

    private MaterialDialog.Builder createMessage(){
        return new MaterialDialog.Builder(getContext())
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }
}
