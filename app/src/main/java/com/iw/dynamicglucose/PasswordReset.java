package com.iw.dynamicglucose;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.get_day_mindsets_work.GetDayMindsetsWork;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PasswordReset extends Activity {

    // Declare vars layout
    private ImageButton btnBack;
    private EditText email;
    private Button resetPassword;

    private Services services;

    // Declare other vars
    private MaterialDialog loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_password_reset);

        // Assignment vars layout
        btnBack = findViewById(R.id.btn_back);
        email = findViewById(R.id.inp_email);
        resetPassword = findViewById(R.id.btn_reset_password);

        // Assignment vars config
        Service service = new Service();
        services = service.getService();

        setListeners();
    }

    private void setListeners() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Login.class);
                startActivity(i);
                finish();
            }
        });

        resetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!email.getText().toString().equals("")){
                    sendResetPassword(email.getText().toString());
                }else{
                    MaterialDialog.Builder dialog = createMessage("Warning", "You need to enter an email to continue", "Accept");
                    dialog.build();
                    dialog.show();
                }
            }
        });

    }

    private void sendResetPassword(String email) {
        loader = loader();
        loader.show();
        // Create the call of the service get day mindsets work
        Call<ResponseBody> call = services.reset_password(email);

        // Executing call of the service get day mindsets work
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                MaterialDialog.Builder dialog;
                switch (response.code()){
                    case 200:
                        loader.dismiss();
                        dialog = createMessage("Success", "Please go to mail to continue with the reset", "Ok");
                        dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Intent i = new Intent(getApplicationContext(), Login.class);
                                startActivity(i);
                                finish();
                            }
                        });
                        dialog.build();
                        dialog.show();
                        break;
                    case 404:
                        loader.dismiss();
                        dialog = createMessage("Success", "Please go to mail to continue with the reset", "Ok");
                        dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Intent i = new Intent(getApplicationContext(), Login.class);
                                startActivity(i);
                                finish();
                            }
                        });
                        dialog.build();
                        dialog.show();
                        break;
                    default:
                        loader.dismiss();
                        dialog = createMessage("Error", "Server not available, try again later", "Accept");
                        dialog.build();
                        dialog.show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loader.dismiss();
                MaterialDialog.Builder dialog = createMessage("Error", "Server not available, try again later", "Accept");
                dialog.build();
                dialog.show();
            }
        });
    }

    private MaterialDialog.Builder createMessage(String title, String content, String positiveText){
        return new MaterialDialog.Builder(this)
                .title(title)
                .content(content)
                .positiveText(positiveText);
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

}
