
package com.iw.dynamicglucose.services.equipment;

import java.util.List;

public class Equipment {

    private List<Equipment_> equipments = null;

    public List<Equipment_> getEquipments() {
        return equipments;
    }

    public void setEquipments(List<Equipment_> equipments) {
        this.equipments = equipments;
    }

}
