package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iw.dynamicglucose.ExtraObjects.Workout;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.process_workout.ProcessWorkout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IW on 31/01/2018.
 */

public class AdapterWorkoutHome extends BaseAdapter {
    private LayoutInflater inflator;
    private Context context;
    private ArrayList<Workout> workouts;
    private Services services;
    private Session session;
    private UserSession user;

    AdapterWorkoutHome(Context context, ArrayList<Workout> workouts){
        inflator = LayoutInflater.from(context);
        this.workouts = workouts;
        this.context = context;
        // Assignment vars config
        Service service = new Service();
        services = service.getService();
        session = new Session(context);
        user = session.getSession();

    }

    public int getCount(){
        return workouts.size();
    }

    public Object getItem(int position){
        return null;
    }

    public long getItemId(int position) {

        return 0;
    }

    @SuppressLint({"ViewHolder", "SetTextI18n", "InflateParams"})
    public View getView(int position, View convertView, ViewGroup parent){
        final Workout workout = workouts.get(position);
        View workout_convertView = inflator.inflate(R.layout.cell_workouts, null);
        ImageView img = workout_convertView.findViewById(R.id.img_workout);
        TextView title = workout_convertView.findViewById(R.id.txt_title_workout);
        TextView time = workout_convertView.findViewById(R.id.txt_time_workout);
        TextView content = workout_convertView.findViewById(R.id.txt_content_workout);

        title.setText("Recommended for you");
        time.setText(workout.getTime() + "s");
        content.setText(workout.getContent());
        Picasso.with(context).load(workout.getImg()).into(img);

        workout_convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (workout.getDaytoday()){
                    Bundle arguments = new Bundle();
                    arguments.putString("id", workout.getId().toString());
                    fragment_single_workout2 fragment = fragment_single_workout2.newInstance(arguments);
                    FragmentTransaction ft = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame_layout, fragment);
                    ft.commit();
                }else{
                    // Create the call of the service time to start
                    Call<ProcessWorkout> call = services.process_workout(workout.getId(), user.getId());

                    // Executing call of the service time to start
                    call.enqueue(new Callback<ProcessWorkout>() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onResponse(Call<ProcessWorkout> call, Response<ProcessWorkout> response) {
                            switch (response.code()){
                                case 200:
                                    if(response.body().getSteps().size() == 1){
                                        Bundle arguments = new Bundle();
                                        arguments.putString("title", response.body().getSteps().get(0).getTitle());
                                        arguments.putString("media", response.body().getSteps().get(0).getMedia());
                                        arguments.putString( "content", response.body().getSteps().get(0).getStep());
                                        arguments.putString("time", response.body().getDuration());
                                        arguments.putInt("workout_id", workout.getId());
                                        arguments.putBoolean("done", response.body().getDone());
                                        fragment_workout_video fragment = fragment_workout_video.newInstance(arguments);
                                        FragmentTransaction ft = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
                                        ft.replace(R.id.frame_layout, fragment);
                                        ft.commit();
                                    }else{
                                        Bundle arguments = new Bundle();
                                        arguments.putString("id", workout.getId().toString());
                                        fragment_steps_workout fragment = fragment_steps_workout.newInstance(arguments);
                                        FragmentTransaction ft = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
                                        ft.replace(R.id.frame_layout, fragment);
                                        ft.commit();
                                    }

                                default:
                                    break;
                            }
                        }

                        @Override
                        public void onFailure(Call<ProcessWorkout> call, Throwable t) {
                            Log.e("Error", "call: " + t.getMessage());
                        }
                    });



                }

            }
        });
        return workout_convertView;

    }

}