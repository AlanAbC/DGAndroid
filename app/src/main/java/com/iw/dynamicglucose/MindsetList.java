package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.today_mindset.TodayMindset;
import com.iw.dynamicglucose.services.view_onboarding_articles.Article;
import com.iw.dynamicglucose.services.view_onboarding_articles.ViewOnboardingArticles;
import com.iw.dynamicglucose.services.view_progress_program.ViewProgramProgress;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MindsetList extends Fragment {

    private LinearLayout list;
    private Service service;
    private Services services;
    private Session session;
    private RecyclerView recyclerView;
    private ImageView back_btn;
    private BottomNavigationView bottomNavigationView;
    private TextView txtDays;


    private List<Article> articles;
    private ArrayList<TodayMindset> mindsets;

    public MindsetList() {
        // Required empty public constructor
    }
    public static MindsetList newInstance() {
        MindsetList fragment = new MindsetList();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mindset_list, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        service = new Service();
        services = service.getService();
        session = new Session(getContext());
        list = getView().findViewById(R.id.container_mindsets);
        recyclerView = getView().findViewById(R.id.list_articles);
        bottomNavigationView = ((Home)getActivity()).findViewById(R.id.navigation);
        back_btn = getView().findViewById(R.id.back_button_meals);
        txtDays = getView().findViewById(R.id.lbl_days);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        loadArticles();
        loadMindsets();
        loadProgramProgress();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuItem item = bottomNavigationView.getMenu().getItem(3);
                item.setChecked(true);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, fragment_mindset.newInstance());
                transaction.commit();
            }
        });
    }

    public void loadMindsets(){
        Call<ArrayList<TodayMindset>> call = services.today_mindset(session.getSession().getId());
        call.enqueue(new Callback<ArrayList<TodayMindset>>() {
            @Override
            public void onResponse(Call<ArrayList<TodayMindset>> call, Response<ArrayList<TodayMindset>> response) {
                switch (response.code()){
                    case 200:
                        mindsets = response.body();
                        setList();
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<ArrayList<TodayMindset>> call, Throwable t) {

            }
        });
    }

    public void loadArticles(){
        Call<ViewOnboardingArticles> call = services.view_onboarding_articles(session.getSession().getId());
        call.enqueue(new Callback<ViewOnboardingArticles>() {
            @Override
            public void onResponse(Call<ViewOnboardingArticles> call, Response<ViewOnboardingArticles> response) {
                switch (response.code()){
                    case 200:
                        articles = response.body().getArticles();
                        recyclerView.setAdapter(new AdapterArticlesMindset(getContext(), articles, response.body().getDisableReason()));
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<ViewOnboardingArticles> call, Throwable t) {

            }
        });
    }

    public void setList(){
        for(final TodayMindset t: mindsets){
            @SuppressLint("InflateParams") RelativeLayout cell_articles = (RelativeLayout) getLayoutInflater().inflate(R.layout.cell_mindset_video, null);
            cell_articles.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle arguments = new Bundle();
                    arguments.putInt("mindset_id", t.getId());
                    SingleFragmentSimple fragment = SingleFragmentSimple.newInstance(arguments);
                    FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame_layout, fragment);
                    ft.commit();
                }
            });
            TextView txt_cell_mindset = cell_articles.findViewById(R.id.mindset_name);
            txt_cell_mindset.setText(t.getTitle());
            list.addView(cell_articles);
        }
    }

    private void loadProgramProgress(){
        // Create the call of the service view program progress
        Call<ViewProgramProgress> call = services.view_program_progress(session.getSession().getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ViewProgramProgress>() {
            @Override
            public void onResponse(Call<ViewProgramProgress> call, Response<ViewProgramProgress> response) {
                switch (response.code()){
                    case 200:
                        ViewProgramProgress data = response.body();
                        assert data != null;
                        if(data.getIs_active()){
                            txtDays.setText(data.getProgress_program());
                        }else{
                            session.CloseSession();
                            Intent i = new Intent(getContext(), Login.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            getActivity().finish();
                        }

                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<ViewProgramProgress> call, Throwable t) {
            }
        });
    }
}
