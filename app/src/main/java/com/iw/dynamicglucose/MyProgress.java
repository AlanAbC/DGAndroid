package com.iw.dynamicglucose;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.get_weight.GetWeight;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyProgress extends AppCompatActivity {

    private LineChart lineChart;
    private MaterialDialog dialog;

    private Services services;
    private UserSession user;

    //vars weight
    private String weightValue;
    private String valueInteger;
    private String valueDecimal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_progress);
        getSupportActionBar().hide();

        //init config vars
        Session session = new Session(getApplicationContext());
        user = session.getSession();
        Service service = new Service();
        services = service.getService();

        //init layout vars
        lineChart = findViewById(R.id.lineChart);
        CircleImageView imageView = findViewById(R.id.imageusr_progrs);
        ImageButton imageButton = findViewById(R.id.refresh);
        Button setWeightBtn = findViewById(R.id.set_weight);


        //init var weight value
        weightValue = "";
        valueInteger = "";
        valueDecimal = "";


        Picasso.with(getApplicationContext()).load(user.getAvatar()).into(imageView);

        loadWeight();

        //listener of refresh
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadWeight();
            }
        });
        setWeightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog = new MaterialDialog.Builder(MyProgress.this)
                        .customView(R.layout.weigh_picker, false)
                        .build();
                dialog.show();
                View viewDialog = dialog.getCustomView();
                assert viewDialog != null;
                NumberPicker intWeight = viewDialog.findViewById(R.id.spinner_weight_int);
                intWeight.setMinValue(100);
                intWeight.setMaxValue(400);
                intWeight.setWrapSelectorWheel(true);
                intWeight.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                        valueInteger = String.valueOf(i1);
                    }
                });

                NumberPicker decimalWeight = viewDialog.findViewById(R.id.spinner_weight_decimals);
                decimalWeight.setMinValue(0);
                decimalWeight.setMaxValue(9);
                decimalWeight.setWrapSelectorWheel(true);
                decimalWeight.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                        valueDecimal = String.valueOf(i1);

                    }
                });

                NumberPicker libs = viewDialog.findViewById(R.id.spinner_weight_libs);
                libs.setMinValue(0);
                libs.setMaxValue(0);
                libs.setDisplayedValues(new String[] {"libs."});

                Button registerWeight = viewDialog.findViewById(R.id.register_new_weight_button);
                registerWeight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (valueInteger.equals("")) {
                            valueInteger = "132";
                        }

                        if(valueDecimal.equals("")) {
                            valueDecimal = "0";
                        }

                        weightValue = valueInteger + "." + valueDecimal;

                          setWeight();
                    }
                });
            }
        });



    }


    private void loadWeight() {

        // Create the call of the service time to start
        Call<GetWeight> call = services.get_weight(user.getId());

        // Executing call of the service time to start
        call.enqueue(new Callback<GetWeight>() {
            @Override
            public void onResponse(Call<GetWeight> call, Response<GetWeight> response) {
                switch (response.code()) {
                    case 200:
                        GetWeight getWeight = response.body();
                        assert getWeight != null;
                        String[] days = new String[getWeight.getWeights().size()];
                        List<Entry> entries = new ArrayList<>();
                        for (int i = 0; i < getWeight.getWeights().size(); i++) {
                            entries.add(new Entry((float) i + 1, getWeight.getWeights().get(i).getWeight().floatValue()));
                            days[i] = ("Day " + (i));
                        }

                        lineChart.getDescription().setEnabled(false);

                        //Set styling data
                        LineDataSet dataSet = new LineDataSet(entries, "");
                        dataSet.setCircleRadius(5);
                        dataSet.setColor(Color.GRAY);
                        dataSet.setCircleColorHole(Color.RED);
                        dataSet.setCircleColor(Color.RED);
                        dataSet.setDrawValues(false);

                        //Set styling x axis
                        XAxis xAxis = lineChart.getXAxis();
                        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                        xAxis.setDrawGridLines(false);
                        xAxis.setTextSize(11);
                        xAxis.setTextColor(Color.rgb(74, 74, 74));
                        xAxis.setValueFormatter(new IndexAxisValueFormatter(days));
                        xAxis.setGranularity(1);

                        //Set styling y axys (left)
                        YAxis leftAxis = lineChart.getAxisLeft();
                        leftAxis.setDrawGridLines(false);
                        leftAxis.setTextColor(Color.rgb(74,74,74));
                        leftAxis.setTextSize(11);

                        //Set styling y axys (right)
                        YAxis rightAxis = lineChart.getAxisRight();
                        rightAxis.setDrawGridLines(false);
                        rightAxis.setDrawLabels(false);

                        Legend legend = lineChart.getLegend();
                        legend.setEnabled(false);

                        //Set chart instance
                        LineData lineData = new LineData(dataSet);

                        lineChart.setData(lineData);

                        //Refresh chart
                        lineChart.invalidate();
                        break;
                    case 400:
                        break;
                    case 500:
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<GetWeight> call, Throwable t) {

            }
        });
    }

    private void setWeight() {
        // Create the call of the service view program progress
        Call<ResponseBody> call = services.set_weight(user.getId(), weightValue);

        // Executing call of the service time to start
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        dialog.dismiss();
                        loadWeight();
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                createToast("Error" + t);
            }
        });
    }

    private void createToast(String msg){
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
