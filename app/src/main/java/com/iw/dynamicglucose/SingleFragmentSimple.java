package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.halilibo.bettervideoplayer.BetterVideoCallback;
import com.halilibo.bettervideoplayer.BetterVideoPlayer;
import com.iw.dynamicglucose.config.BaseUrl;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.today_mindset.TodayMindset;
import com.iw.dynamicglucose.services.today_process_mindset.TodayProcessMindset;
import com.iw.dynamicglucose.services.view_progress_program.ViewProgramProgress;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SingleFragmentSimple extends Fragment implements BetterVideoCallback {
    // Declare vars config
    private BaseUrl baseUrl;
    private Services services;
    private Session session;
    private UserSession user;

    private TextView title;
    private TextView time;
    private ImageButton rate1;
    private ImageButton rate2;
    private ImageButton rate3;
    private ImageButton rate4;
    private ImageButton rate5;
    private ImageView back_button_meals;
    private TextView lblrate;
    private TextView content;
    private RelativeLayout showMindset;
    private RelativeLayout hiddenMindset;
    private TextView txtDays;
    private Integer mindset_id;
    // Declare var loader
    private MaterialDialog loader;
    private  MaterialDialog dialogDays;
    private BetterVideoPlayer player;


    public SingleFragmentSimple() {
        // Required empty public constructor
    }

    public static SingleFragmentSimple newInstance(Bundle arguments) {
        SingleFragmentSimple fragment = new SingleFragmentSimple();
        if(arguments != null){
            fragment.setArguments(arguments);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_single_fragment_simple, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //Llamada de variables
        title = getView().findViewById(R.id.txt_title_mindset);
        time = getView().findViewById(R.id.txt_time_mindset);
        rate1 = getView().findViewById(R.id.btn_rate_1_mindset);
        rate2 = getView().findViewById(R.id.btn_rate_2_mindset);
        rate3 = getView().findViewById(R.id.btn_rate_3_mindset);
        rate4 = getView().findViewById(R.id.btn_rate_4_mindset);
        rate5 = getView().findViewById(R.id.btn_rate_5_mindset);
        lblrate = getView().findViewById(R.id.txt_rate_mindset);
        content = getView().findViewById(R.id.txt_content_mindset);
        showMindset = getView().findViewById(R.id.show_mindset);
        hiddenMindset = getView().findViewById(R.id.hidden_mindset);
        back_button_meals = getView().findViewById(R.id.back_button_meals);
        txtDays = getView().findViewById(R.id.lbl_days);
        player = getView().findViewById(R.id.player);

        mindset_id = getArguments().getInt("mindset_id");

        // Assignment vars config
        Service service = new Service();
        services = service.getService();
        session = new Session(getContext());
        user = session.getSession();

        rate1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), mindset_id, 1);
            }
        });
        rate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), mindset_id, 2);
            }
        });
        rate3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), mindset_id, 3);
            }
        });
        rate4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), mindset_id, 4);
            }
        });
        rate5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), mindset_id, 5);
            }
        });

        loadProcessMindset(mindset_id);
        loadProgramProgress();

        back_button_meals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, MindsetList.newInstance());
                transaction.commit();
            }
        });
    }

    private void rate(Integer patient_id, Integer mindset_id, final Integer rating){
        MaterialDialog loader = loader();
        loader.show();

        // Create the call of the service time to start
        Call<ResponseBody> call = services.set_rating_mindset(patient_id,mindset_id,rating);

        // Executing call of the service time to start
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        rate1.setEnabled(false);
                        rate2.setEnabled(false);
                        rate3.setEnabled(false);
                        rate4.setEnabled(false);
                        rate5.setEnabled(false);
                        switch(rating){
                            case 1:{
                                lblrate.setText("1/5");
                                rate1.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 2:{
                                lblrate.setText("2/5");
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 3:{
                                lblrate.setText("3/5");
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                rate3.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 4:{
                                lblrate.setText("4/5");
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                rate3.setImageResource(R.drawable.star_checked);
                                rate4.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 5:{
                                lblrate.setText("5/5");
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                rate3.setImageResource(R.drawable.star_checked);
                                rate4.setImageResource(R.drawable.star_checked);
                                rate5.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            default:{
                                break;
                            }
                        }
                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
                Log.e("errorCall", t.getMessage());
            }
        });
        loader.dismiss();
    }

    private void loadProcessMindset(Integer id_mindset){
        // Create the call of the service time to start
        Call<TodayProcessMindset> call = services.today_process_mindset(id_mindset, user.getId());

        // Executing call of the service time to start
        call.enqueue(new Callback<TodayProcessMindset>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<TodayProcessMindset> call, Response<TodayProcessMindset> response) {
                switch (response.code()){
                    case 200:
                        // Getting data objects
                        TodayProcessMindset data = response.body();
                        assert data != null;
                        player.setCallback(SingleFragmentSimple.this);
                        player.setAutoPlay(true);
                        player.start();
                        player.setSource(Uri.parse(data.getMedia()));
                        title.setText(data.getTitle());
                        time.setText(data.getDuration());
                        content.setText(data.getDescription());
                        lblrate.setText(data.getRating().toString() + "/5");
                        switch (data.getRating()){
                            case 0:{
                                break;
                            }
                            case 1:{
                                rate1.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 2:{
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 3:{
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                rate3.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 4:{
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                rate3.setImageResource(R.drawable.star_checked);
                                rate4.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 5:{
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                rate3.setImageResource(R.drawable.star_checked);
                                rate4.setImageResource(R.drawable.star_checked);
                                rate5.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            default:{
                                break;
                            }
                        }
                        break;

                    case 204:
                        showMindset.setVisibility(View.GONE);
                        hiddenMindset.setVisibility(View.VISIBLE);
                        break;

                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<TodayProcessMindset> call, Throwable t) {
                Log.e("Error", "call: " + t.getMessage());
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }


        });
    }

    private void loadProgramProgress(){
        dialogDays = loader();
        dialogDays.show();
        // Create the call of the service view program progress
        Call<ViewProgramProgress> call = services.view_program_progress(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ViewProgramProgress>() {
            @Override
            public void onResponse(Call<ViewProgramProgress> call, Response<ViewProgramProgress> response) {
                switch (response.code()){
                    case 200:
                        ViewProgramProgress data = response.body();
                        assert data != null;
                        if(data.getIs_active()){
                            txtDays.setText(data.getProgress_program());
                        }else{
                            session.CloseSession();
                            Intent i = new Intent(getContext(), Login.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            getActivity().finish();
                        }

                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialogDays.dismiss();
            }

            @Override
            public void onFailure(Call<ViewProgramProgress> call, Throwable t) {
                dialogDays.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }

    private MaterialDialog.Builder createMessage(){
        return new MaterialDialog.Builder(getContext())
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext())
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

    @Override
    public void onPause() {
        super.onPause();
        player.pause();
    }

    @Override
    public void onStarted(BetterVideoPlayer player) {

    }

    @Override
    public void onPaused(BetterVideoPlayer player) {

    }

    @Override
    public void onPreparing(BetterVideoPlayer player) {

    }

    @Override
    public void onPrepared(BetterVideoPlayer player) {

    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(BetterVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(BetterVideoPlayer player) {

    }

    @Override
    public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {

    }
}
