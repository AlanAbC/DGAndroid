package com.iw.dynamicglucose.ExtraObjects;


import java.util.ArrayList;
import java.util.Map;

/**
 * Created by IW on 13/02/2018.
 */

public class Data {
    private Integer patient_id;
    private Integer category;
    private ArrayList<Map<String,Integer>> data;

    public Data(Integer patient_id, Integer category, ArrayList<Map<String, Integer>> data) {
        this.patient_id = patient_id;
        this.category = category;
        this.data = data;
    }

    public Data(){

    }
    public ArrayList<Map<String, Integer>> getData() {
        return data;
    }

    public void setData(ArrayList<Map<String, Integer>> data) {
        this.data = data;
    }

    public Integer getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }
}
