package com.iw.dynamicglucose.config;

/**
 * Created by IW on 29/ene/2018.
 */

public class BaseUrl {

    //private static String url = "http://192.168.15.84:8000/api/";
    //private static String urlSocket = "ws://192.168.16.113:8000/programs/chat/";
    //private static String baseurl = "http://192.168.16.113:8000/";
    //private static String url = "http://192.168.16.113:8000/api/";
    //Servidores de Amazon
    private static String url = "https://dynamicglucose.com/admin/api/";
    private static String baseurl = "https://dynamicglucose.com/admin/";
    private static String urlSocket = "wss://dynamicglucose.com/programs/chat/";
    //private static String url = "http://soporteiw.iwsandbox.com:8000/api/";
    //private static String baseurl = "http://soporteiw.iwsandbox.com:8000/";
    public BaseUrl(){}

    public String getUrl(){
        return url;
    }

    public String getUrlSocket(){return urlSocket;
    }

    public String getBaseurl() {
        return baseurl;
    }
}
