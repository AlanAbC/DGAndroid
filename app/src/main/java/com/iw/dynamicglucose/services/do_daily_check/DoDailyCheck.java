
package com.iw.dynamicglucose.services.do_daily_check;


public class DoDailyCheck {

    private Boolean daily_check;
    private Boolean weigh_in;

    public Boolean getDaily_check() {
        return daily_check;
    }

    public void setDaily_check(Boolean daily_check) {
        this.daily_check = daily_check;
    }

    public Boolean getWeigh_in() {
        return weigh_in;
    }

    public void setWeigh_in(Boolean weigh_in) {
        this.weigh_in = weigh_in;
    }

}
