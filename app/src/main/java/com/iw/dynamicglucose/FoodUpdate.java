package com.iw.dynamicglucose;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

public class FoodUpdate extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_update);
        getSupportActionBar().hide();
        RelativeLayout meat = findViewById(R.id.meats_update);
        RelativeLayout veggies = findViewById(R.id.veggies_update);
        RelativeLayout sauces = findViewById(R.id.sauces_update);
        RelativeLayout products = findViewById(R.id.products_update);
        ImageButton back = findViewById(R.id.back_food_update);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        meat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), OnboardingMeats.class);
                i.putExtra("settings", true);
                startActivity(i);
            }
        });
        veggies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), OnboardingVegetables.class);
                i.putExtra("settings", true);
                startActivity(i);
            }
        });
        sauces.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), OnboardingFruits.class);
                i.putExtra("settings", true);
                startActivity(i);
            }
        });
        products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), OnboardingProducts.class);
                i.putExtra("settings", true);
                startActivity(i);
            }
        });
    }
}
