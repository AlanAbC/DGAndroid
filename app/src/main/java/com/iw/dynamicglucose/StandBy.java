package com.iw.dynamicglucose;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class StandBy extends AppCompatActivity {
    private TextView days;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stand_by);

        getSupportActionBar().hide();

        days = findViewById(R.id.days_counter);

        Integer totaldays = getIntent().getIntExtra("days", 0) - 7;
        days.setText(totaldays.toString());
    }
}
